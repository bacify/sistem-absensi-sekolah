<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Musers; 
 

class Login extends BaseController
{
	public function index()
    {
        if(session()->get('logged_in')){          
            return redirect()->to('/panel/home/');
            
        }
        helper(['form']);
        echo view('login');
    }
    public function te()
    {
        
        helper(['form']);
        echo view('test');
    } 
    public function auth()
    {
        
        $session = session();
       
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $m = new Musers();
        $d= $m->where('username',$username)->first();
         
        
        
        if($d){
            $pass = $d['password'];
            $verify_pass = ($password === $pass);
            if($verify_pass){
                $ses_data = [
                    'uid' => $d['uid'],                     
                    'nama'     => $d['nama'],                    
                    'email'     => $d['email'],                    
                    'akses'     => $d['akses'],                    
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/panel/home/');
            }else{
                $session->setFlashdata('error', 'Wrong Password');                
                return redirect()->to('/login');
            }
        }else{
            
            $session->setFlashdata('error', 'Username not Found');
            
            return redirect()->to('/login');
        }               

    }


    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

     

}