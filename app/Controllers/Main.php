<?php

namespace App\Controllers;
use App\Models\Mabsen; 
use App\Models\Mkelas; 
use App\Models\Msetting; 
use App\Models\Msiswa; 
use App\Models\Musers; 
use App\Models\Mnotifikasi; 
use \Hermawan\DataTables\DataTable;
use App\Libraries\Api_wa; // Import library
 

class Main extends BaseController
{
	public function index()
	{
         
        $data['title']='Dashboard Absensi Siswa';
        $data['status']=1;
        $s = new Msetting();
        $setting = $s->first();
        $result =array();      
        $s1 = new \Datetime($setting['jam_masuk']);
        $s2 = new \Datetime(date('H:i:s'));
        if($setting['batas']>0){                
            if ($s1 < $s2){
                $data['status']=0;               
            }else{
                $s1->modify('+'.$setting['batas'].' minutes');
                if($s1 < $s2){
                    $data['status']=0;
                }
            }
             
        }
        if($s1>$s2)
        $data['status']=0;


    // check status Libur
    // JIKA LIBUR
                    $s = new Msetting();
                    $setting = $s->first();
                    
                    $libur = $setting['hari_libur'];
                    $hari = explode(',',$libur);

                    $listday=array();
                    if($hari[0] == 1)
                        $listday[]=1;
                    if($hari[1] == 1)
                        $listday[]=2;
                    if($hari[2] == 1)
                        $listday[]=3;
                    if($hari[3] == 1)
                        $listday[]=4;
                    if($hari[4] == 1)
                        $listday[]=5;
                    if($hari[5] == 1)
                        $listday[]=6;
                    if($hari[6] == 1)
                        $listday[]=0;
                    
                    $db      = \Config\Database::connect();
                    $builder = $db->table('tanggal_libur')
                                    ->select('id_tanggal,tanggal' ); 
                                    $query = $builder->get();
                    $tanggal_libur=array();
                    foreach ($query->getResult() as $row) {
                        $tanggal_libur []= $row->tanggal;
                    }
                    $data['libur']=0;
                    if(in_array(date('w'), $listday) || in_array(date('Y-m-d'),$tanggal_libur)){
                        $data['libur']=1;
                    }


		return view('home',$data);
    }
    public function nfc(){
        $data['title']='Dashboard Absensi Siswa';
		return view('home_nfc',$data);
    }
    public function home(){
        $data['title']='Dashboard Absensi Siswa';
        $data['status']=1;
        $s = new Msetting();
        $setting = $s->first();
        $result =array();      
        $s1 = new \Datetime($setting['jam_masuk']);
        $s2 = new \Datetime(date('H:i:s'));
        if($setting['batas']>0){                
            if ($s1 < $s2){
                $data['status']=0;               
            }else{
                $s1->modify('+'.$setting['batas'].' minutes');
                if($s1 < $s2){
                    $data['status']=0;
                }
            }
             
        }
        if($s1>$s2)
        $data['status']=0;

        return view('home',$data);
    }
    public function kelas()
	{
        

        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        $data['token']=$token;
        $data['title']='Data kelas';
       
		return view('data_kelas',$data);
    }
    public function kelas_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('kelas')
                        ->select('id_kelas,nama_kelas')                        
                        ->where('kelas.deleted_at IS NULL');
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                        return  '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm" data-id="'.$row->id_kelas.'" data-nama="'.$row->nama_kelas.'"  title="Edit Transaksi"><i class="cil-pencil"></i></a> '.
                            ' <a href="javascript:void(0);" class="delete_record btn btn-outline-danger btn-sm" data-id="'.$row->id_kelas.'" title="Hapus Transaksi"> <i class="cil-trash"></i></a>';                   
                    
                }, 'last')                 
                ->hide('id_kelas')
                ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }
    public function kelas_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Mkelas();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('error', 'Kelas berhasil dihapus');
			}	else{
                $session->setFlashdata('error', 'Kelas GAGAL dihapus');
			}
		}			

        return redirect()->to('panel/dkelas');
    }
    public function kelas_add(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                 
                                'token' => 'required',                                
                                'nama' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');                
                    $value=array();
                    $value['id_kelas']=$this->request->getPost('ids');
                    $value['nama_kelas']=$this->request->getPost('nama');
                    $value['nis']=$this->request->getPost('nis');
                     
                    $m = new Mkelas();
                    $result = $m->save($value);
                    if($result){
                        if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                            $id=$m->getInsertID();
                            //echo 'valid';
                            $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                        }else{                         
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                        }                    
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/dkelas');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/dkelas');
    }
     

    public function siswa()
	{
        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Data Siswa';
		return view('data_siswa',$data);
    }
    public function siswa_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('siswa')
                        ->select('id_siswa,nama,nis,nama_kelas,alamat,no_telp,kelas,notifikasi')
                        ->join('kelas','kelas.id_kelas = siswa.kelas ')
                        ->where('siswa.deleted_at IS NULL');
                         

         
        return DataTable::of($builder)   
                ->edit('notifikasi', function($row){
                    if($row->notifikasi==1)
                        return '<span class="badge badge-success">ON</span>';                    
                    else 
                        return '<span class="badge badge-danger">OFF</span>';
                    
                })                  
                ->add('action', function($row){ 
                        return  '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm " data-id="'.$row->id_siswa.'" data-id="'.$row->id_siswa.'" data-nama="'.$row->nama.'" data-nis="'.$row->nis.'" data-kelas="'.$row->kelas.'" data-telp="'.$row->no_telp.'" data-alamat="'.$row->alamat.'" data-notifikasi="'.$row->notifikasi.'" title="Edit User"><i class="cil-pencil"></i></a>'.                            
                            ' <a href="javascript:void(0);" class="delete_record btn btn-outline-danger btn-sm " data-id="'.$row->id_siswa.'" title="Hapus Transaksi"><i class="cil-trash"></i></a>';                   
                    
                }, 'last')                 
                ->hide('id_siswa')
                ->hide('kelas')
                ->addNumbering() //it will return data output with numbering on first column
                ->filter(function ($builder, $request) {
        
                    if ($request->kelas)
                        $builder->where('siswa.kelas', $request->kelas);
            
                })
               ->toJson();
    }

    public function siswa_nis_check(){
        if( $this->request->getPost('nis') !== NULL){
			$con=array();
            $con['nis']=$this->request->getPost('nis');
            $u= new Msiswa();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }
    public function siswa_add(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nis' => 'required|numeric',
                                'token' => 'required',
                                'telp' => 'required|numeric',
                                'nama' => 'required',
                                'notifikasi' => 'required'

                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                $con=array();
                $con['nis']=$this->request->getPost('nis');
                $m = new Msiswa();
                $x = $m->where($con)->findAll();

                if($this->request->getPost('ids') == '' && count($x)>0){
                    $session->setFlashdata('error', 'Identitas Sudah Dipakai');  
                }else{
                    // GET WA NUMBER 
                    $set = new Msetting();
                    $setting = $set->first();
                    
                    if($setting['wa_status']==1){
                        //echo 'aaaaa';
                        $wa = new Api_wa();
                        $r=$wa->ceknomor($this->request->getPost('telp'));
                        $rx= json_decode($r);
                         
                        if($rx->status == 'false'){
                             
                            $session->setFlashdata('error', 'No '.$rx->pesan);  
                            return redirect()->to('/panel/dsiswa');
                        }

                    }
                     
                        $value=array();
                        $value['id_siswa']=$this->request->getPost('ids');
                        $value['nama']=$this->request->getPost('nama');
                        $value['nis']=$this->request->getPost('nis');
                        $value['alamat']=$this->request->getPost('alamat');
                        $value['kelas']=$this->request->getPost('kelas');
                        $value['no_telp']=$this->request->getPost('telp'); 
                        $value['notifikasi']=$this->request->getPost('notifikasi'); 
                        $m = new Msiswa();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                                $id=$m->getInsertID();

                               // $this->notifikasi_pendaftaran($value['no_telp'],$value['nama']);
                                $setting = new Msetting();
                                $sett = $setting->first();
                                $arrdata= array('instansi'=>$sett['nama_perusahaan'],
                                                'nama' =>$value['nama'],
                                                'telp_instansi'=>'0',
                                                'kelas'=>$value['kelas'],
                                                'nis'=>$value['nis']);
                                $p = $this->m_pesan(2,$arrdata);
                                if($p['status']== 1 && $p['enable'] == 1){
                                    //kirim notifikasi pendaftaran 
                                    $wa = new Api_wa();
                                     $wa->send_wa($value['no_telp'],$p['pesan']);
                                }else{

                                }
                                
                                


                                //echo 'valid';
                                $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                            }else{                         
                                $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                            }                    
                        }else{
                            $session->setFlashdata('error', 'Update Data GAGAL');  
                        }
                    
                }
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/dsiswa');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/dsiswa');
    }
    
    public function siswa_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Msiswa();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('pesan', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'user GAGAL dihapus');
			}
		}

			

		return redirect()->to('panel/dsiswa');
    }
    public function siswa_json($id){
        if($id==null)
		{
            $result['status']=0;
            $result['result']=array();;
            die();
        }	
        $m = new Msiswa();
		$user=$m->find($id);
		$result=array();
		if($user!=''){
			$result['status']=1;
			$result['result']=$user;
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
    }
    public function siswa_search(){
        $con=$this->request->getGet('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		
		$like['nama'] = $con;		
        $cs= new Msiswa();    
        $r = $cs->select('id_member,
                         nama,telp')->like($like)
                         ->where('deleted_at IS null')
                        ->findAll(10);
       
                $result=array();
                foreach($r as $rr){
                    $temp=array();
                    
                    $temp=array('value'=>$rr['id_member'], 'label'=>$rr['nama'].'| '.$rr['telp']);
                    $result[]=$temp;
                };
                

        header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function daftar_absensi()
	{
        

        $begin = new \DateTime( '-3 day' );
        $end = new \DateTime( 'now' );        
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);        
        $t=array();
        foreach($daterange as $date){
            $t[]=array('input'=>$date->format("Y-m-d"), 'value'=>$date->format("d-m-Y"));

        }      

        $t[]=array('input'=>$end->format("Y-m-d"), 'value'=>$end->format("d-m-Y"));;
        $data['tanggal']=array_reverse($t);

        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
        
        $data['token']=$token;
		$data['title']='Data Absensi';
		return view('data_absensi',$data);
    }
    public function absensi_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('siswa_kalender')
                        ->select('id_absensi,nama,nis,nama_kelas,COALESCE(kehadiran,tanggal) as kh,COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") as absen,id_siswa' )
                        ->join('absensi_harian','absensi_harian.siswa_id = siswa_kalender.id_siswa AND date(absensi_harian.kehadiran) = date(siswa_kalender.tanggal)','LEFT')                                                 
                        ->join('kelas','kelas.id_kelas = siswa_kalender.kelas ')  
                        ->where('siswa_kalender.kelas', $this->request->getGet('kelas'))						
						->where('date(tanggal)', $this->request->getGet('tanggal'))						                                           
                        ->where('siswa_kalender.deleted_at IS NULL')                        
                        ->where('absensi_harian.deleted_at IS NULL');
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                    if($row->absen == 'X')
                        return  'Libur';    

                    if($row->id_absensi == '')
                        return  '<a href="javascript:void(0);" class="add_record btn btn-info btn-sm"  data-id="'.$row->id_siswa.'" title="Tambahkan Keterangan"><i class="cil-note-add"></i></a>';                           
                    else                        
                        return  ' <a href="javascript:void(0);" class="edit_record btn btn-info btn-sm " data-id="'.$row->id_siswa.'" data-idabsen="'.$row->id_absensi.'" title="Edit keterangan"><i class="cil-info"></i></a>';                   
                    
                }, 'last')   
                ->edit('absen', function($row){
                    if($row->absen=='A')
                        return '<span class="badge badge-danger">'.$row->absen.'</span>';
                    else if($row->absen=='I')
                        return '<span class="badge badge-primary">'.$row->absen.'</span>';
                    else if($row->absen=='S')
                        return '<span class="badge badge-info">'.$row->absen.'</span>';
                    else 
                        return '<span class="badge badge-success">'.$row->absen.'</span>';
                    
                })              
                ->hide('id_absensi')                
                ->hide('id_siswa')                
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }

    public function absensi_siswa(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nis' => 'required'                                 
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
                $nis= $this->request->getPost('nis');                 

                $m = new Msiswa();
                        $siswa = $m->where(array('nis'=>$nis))->first();
                         
                        if($siswa == ''){
                            $result['status'] = 0;
                            $result['text'] = 'Gagal! Data Tidak Ditemukan';
                        }else{
                            /* insert ABSENSI */

                            $a = new Mabsen();
                            $absen = $a->where('date(kehadiran)','date(NOW())',false)->where('siswa_id',$siswa['id_siswa'])->first();
                            
                            if($absen != ''){
                                $result['status'] = 0;
                                $result['text'] = 'Sudah Absen';  
                            }else{
                                $val = array('siswa_id'=>$siswa['id_siswa'],
                                            'keterangan'=>'M',
                                            'user_id'=>session()->get('uid'),
                                            'created_by'=>'AUTO');
                                $r=$a->save($val);
                                if($r){
                                    $result['status']=1;
                                    $result['siswa']=$siswa;
                                    $result['text'] = 'Berhasil';
                                }else{
                                    $result['status']=0;
                                    $result['siswa']=$siswa;
                                    $result['text'] = 'Gagal';
                                }
                            }
                        }
                
        }
        else{
            $errors = $validation->getErrors();
            $result['status']=0;            
            $result['text'] =json_encode($errors);            
        }

        header('Content-Type: application/json');		
		echo  json_encode($result);
    }

    public function absensi_manual(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'ids' => 'required|numeric',
                                'token' => 'required',
                                'created_by' => 'required',
                                'keterangan' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                
                 
                    
                     
                        $value=array();
                        $value['id_absensi']=$this->request->getPost('id_absen');
                        $value['siswa_id']=$this->request->getPost('ids');
                        $value['keterangan']=$this->request->getPost('keterangan');
                        $value['created_by']=$this->request->getPost('created_by');
                        $value['user_id']=session()->get('uid');
                         
                        $m = new Mabsen();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('id_absen') == '' || $this->request->getPost('id_absen') == '0'){
                                $id=$m->getInsertID();                                 
                                //echo 'valid';
                                $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                            }else{                         
                                $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                            }                    
                        }else{
                            $session->setFlashdata('error', 'Update Data GAGAL');  
                        }
                    
               
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('panel/absensi');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/absensi');
    }
    public function absensi_manual_ajax(){
        $result_ajax=array();
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'ids' => 'required|numeric',                                
                                'created_by' => 'required',
                                'keterangan' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            
                
                 
                    
                     
                        $value=array();
                        $value['id_absensi']=$this->request->getPost('id_absen');
                        $value['siswa_id']=$this->request->getPost('ids');
                        $value['keterangan']=$this->request->getPost('keterangan');
                        
                        if($this->request->getPost('tanggal_absen'))
                        $value['kehadiran']=$this->request->getPost('tanggal_absen');
                        $value['created_by']=$this->request->getPost('created_by');                        
                        $value['user_id']=session()->get('uid');
                         
                        $m = new Mabsen();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('id_absen') == '' || $this->request->getPost('id_absen') == '0'){
                                $id=$m->getInsertID();                                 
                                //echo 'valid';
                                $result_ajax['pesan']='Data berhasil disimpan '.$id;
                                $result_ajax['status']=1;
                            }else{  
                                $result_ajax['pesan']='Perubahan Data berhasil dilakukan';
                                $result_ajax['status']=1;
                            }                    
                        }else{
                            
                            $result_ajax['pesan']='Update Data GAGAL';
                            $result_ajax['status']=0;
                        }
                    
               
            
        }
        else{
            $errors = $validation->getErrors();
            $result_ajax['pesan']='GAGAL. error:'.json_encode($errors);    
            $result_ajax['status']=0;                
             
        }

                   
        echo json_encode($result_ajax);
    }
    public function daftar_laporan()
	{
        $db = db_connect();
        $query = $db->query("select min(YEAR(tanggal)) as t from kalender");
        $r = $query->getResult();
         $min = $r[0]->t;
        $n = range($min,date('Y'));
        $data['tahun']=$n;
        $bulan = array(
                      ['input' => '1','value'=>'Januari'],
                      ['input' => '2','value'=>'Februari'],
                      ['input' => '3','value'=>'Maret'],
                      ['input' => '4','value'=>'April'],
                      ['input' => '5','value'=>'Mei'],
                      ['input' => '6','value'=>'Juni'],
                      ['input' => '7','value'=>'Juli'],
                      ['input' => '8','value'=>'Agustus'],
                      ['input' => '9','value'=>'September'],
                      ['input' => '10','value'=>'Oktober'],
                      ['input' => '11','value'=>'November'],
                      ['input' => '12','value'=>'Desember'],
                    );
         
        $data['bulan'] = $bulan;        

        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
         $s=new Msetting();
        $data['setting']=$s->first();
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Laporan Absensi Siswa ';
		return view('data_laporan',$data);
    }
    public function daftar_laporan_manual()
	{
        $db = db_connect();
        $query = $db->query("select min(YEAR(tanggal)) as t from kalender");
        $r = $query->getResult();
         $min = $r[0]->t;
        $n = range($min,date('Y'));
        $data['tahun']=$n;
        $bulan = array(
                      ['input' => '1','value'=>'Januari'],
                      ['input' => '2','value'=>'Februari'],
                      ['input' => '3','value'=>'Maret'],
                      ['input' => '4','value'=>'April'],
                      ['input' => '5','value'=>'Mei'],
                      ['input' => '6','value'=>'Juni'],
                      ['input' => '7','value'=>'Juli'],
                      ['input' => '8','value'=>'Agustus'],
                      ['input' => '9','value'=>'September'],
                      ['input' => '10','value'=>'Oktober'],
                      ['input' => '11','value'=>'November'],
                      ['input' => '12','value'=>'Desember'],
                    );
         
        $data['bulan'] = $bulan;        

        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Laporan Absensi Siswa ';
		return view('data_laporan_manual',$data);
    }
    public function laporan_absensi_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('absensi')
                        ->select('id_siswa,id_absensi,nis,nama,nama_kelas,
                                max(case when day(tanggal) = 1 AND date(tanggal) <= date(NOW())  then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A1",
                                max(case when day(tanggal) = 2 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A2",
                                max(case when day(tanggal) = 3 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A3",
                                max(case when day(tanggal) = 4 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A4",
                                max(case when day(tanggal) = 5 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A5",
                                max(case when day(tanggal) = 6 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A6",
                                max(case when day(tanggal) = 7 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A7",
                                max(case when day(tanggal) = 8 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A8",
                                max(case when day(tanggal) = 9 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A9",
                                max(case when day(tanggal) = 10 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A10",
                                max(case when day(tanggal) = 11 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A11",
                                max(case when day(tanggal) = 12 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A12",
                                max(case when day(tanggal) = 13 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A13",
                                max(case when day(tanggal) = 14 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A14",
                                max(case when day(tanggal) = 15 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A15",
                                max(case when day(tanggal) = 16 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A16",
                                max(case when day(tanggal) = 17 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A17",
                                max(case when day(tanggal) = 18 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A18",
                                max(case when day(tanggal) = 19 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A19",
                                max(case when day(tanggal) = 20 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A20",
                                max(case when day(tanggal) = 21 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A21",
                                max(case when day(tanggal) = 22 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A22",
                                max(case when day(tanggal) = 23 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A23",
                                max(case when day(tanggal) = 24 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A24",
                                max(case when day(tanggal) = 25 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A25",
                                max(case when day(tanggal) = 26 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A26",
                                max(case when day(tanggal) = 27 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A27",
                                max(case when day(tanggal) = 28 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A28",
                                max(case when day(tanggal) = 29 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A29",
                                max(case when day(tanggal) = 30 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A30",
                                max(case when day(tanggal) = 31 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(siswa_kalender.tanggal)),"X",null),keterangan,"A") end) as "A31"
                                 ')
                        ->join('siswa_kalender','absensi.siswa_id = siswa_kalender.id_siswa and date(absensi.kehadiran) = date(siswa_kalender.tanggal)','right')
                        ->join('kelas','kelas.id_kelas = siswa_kalender.kelas ')                        
                        ->where('siswa_kalender.deleted_at IS NULL')
                        ->where('absensi.deleted_at IS NULL')  
						->where('Year(tanggal)', $this->request->getGet('tahun'))						
						->where('month(tanggal)', $this->request->getGet('bulan'))
						->where('siswa_kalender.kelas', $this->request->getGet('kelas'))
                        ->groupBy('id_siswa,nama,nis,nama_kelas');
                         
			//print_r($builder->getCompiledSelect());  //print query

					
         
        return DataTable::of($builder) 
                ->hide('id_absensi') 
                ->hide('id_siswa')                     
                ->hide('nama_kelas')                
                ->addNumbering() //it will return data output with numbering on first column
                // ->filter(function ($builder, $request) {        
                    // if ($request->tahun)
                        // $builder->where('Year(tanggal)', $request->tahun);
                    // if ($request->bulan)
                        // $builder->where('month(tanggal)', $request->bulan);  
                    // if ($request->kelas)
                        // $builder->where('siswa_kalender.kelas', $request->kelas);           
                // })
               ->toJson();
			    
    }
    public function get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b){
        $set = new Msetting();
        $setting= $set->first();

        $libur = $setting['hari_libur'];
        $hari = explode(',',$libur);

        $listday=array();
        if($hari[0] == 1)
            $listday[]=1;
        if($hari[1] == 1)
            $listday[]=2;
        if($hari[2] == 1)
            $listday[]=3;
        if($hari[3] == 1)
            $listday[]=4;
        if($hari[4] == 1)
            $listday[]=5;
        if($hari[5] == 1)
            $listday[]=6;
        if($hari[6] == 1)
            $listday[]=0;

        
        
        $t_a = new \DateTime($tahun_a.'-'.$bulan_a.'-01');
        $lastdate = date('t',strtotime($tahun_b.'-'.$bulan_b.'-01'));
       // echo $lastdate;
        $t_b = new \DateTime($tahun_b.'-'.$bulan_b.'-'.$lastdate);
        $t_b = $t_b->modify( '+1 day' );
         
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($t_a, $interval ,$t_b);
        $listbreakdate=array();
        foreach($daterange as $date){
            $day = $date->format('w');

            if(in_array($day, $listday))
            $listbreakdate[] =$date->format('Y-m-d');
        }

        $db = db_connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' );
        $r = $builder->get();     
        $tgl_libur=array();
        foreach ($r->getResult() as $row)
        {
            if (($key = array_search($row->tanggal, $listbreakdate)) !== false) {
                unset($listbreakdate[$key]);
            }

        }    
         
         
        return $listbreakdate;
    }
    public function laporan_absensi_ajax_B()
	{
          
        $tahun_a =$this->request->getGet('tahun');
        $bulan_a =$this->request->getGet('bulan');
        $tahun_b =$this->request->getGet('tahunakhir');
        $bulan_b =$this->request->getGet('bulanakhir');

        // $listbreakdate = $this->get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b);

        
        // $time_b=strtotime($tahun_b.'-'.$bulan_b.'-01');
        // $tanggal_b= date('Y-m-t',$time_b);
        // $listbreak_final=array();
        // foreach($listbreakdate as $lb){
        //     if(date('Y-m-d',strtotime($lb)) <= date('Y-m-d'))
        //     $listbreak_final[]=$lb;
        // }
         
        $db = db_connect();
        $builder = $db->table('laporan_absensi')
                        ->select('id_siswa,nis,nama,nama_kelas,
                        SUM(CASE WHEN absen = "A" AND date(tanggal_absen) <= NOW() THEN 1 ELSE 0 END) AS A, 
                        SUM(CASE WHEN absen = "I" AND date(tanggal_absen) <= NOW() THEN 1 ELSE 0 END) AS I,
                        SUM(CASE WHEN absen = "M" AND date(tanggal_absen) <= NOW() THEN 1 ELSE 0 END) AS M,
                        SUM(CASE WHEN absen = "S" AND date(tanggal_absen) <= NOW() THEN 1 ELSE 0 END) AS S,
                        SUM(CASE WHEN absen = "X" AND date(tanggal_absen) <= NOW() THEN 1 ELSE 0 END) AS X')                        
                        ->join('kelas','kelas.id_kelas = laporan_absensi.kelas ')                        
                        ->where('laporan_absensi.deleted_at IS NULL')   
						->where('Year(tanggal_absen)>=', $this->request->getGet('tahun'))						
						->where('month(tanggal_absen)>=', $this->request->getGet('bulan'))
						->where('Year(tanggal_absen)<=', $this->request->getGet('tahunakhir'))
						->where('month(tanggal_absen)<=', $this->request->getGet('bulanakhir'))
						->where('laporan_absensi.kelas', $this->request->getGet('kelas'))							
                        ->groupBy('id_siswa');
                         
                         

         
        return DataTable::of($builder) 
                ->hide('id_siswa')                
                ->hide('nama_kelas')
                ->edit('X', function($row){
                    $tahun_a =$this->request->getGet('tahun');
                    $bulan_a =$this->request->getGet('bulan');
                    $tahun_b =$this->request->getGet('tahunakhir');
                    $bulan_b =$this->request->getGet('bulanakhir');

                    $listbreakdate = $this->get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b);
                    $time_b=strtotime($tahun_b.'-'.$bulan_b.'-01');
                    $tanggal_b= date('Y-m-t',$time_b);
                    $listbreak_final=array();
                    foreach($listbreakdate as $lb){
                        if(date('Y-m-d',strtotime($lb)) <= date('Y-m-d'))
                        $listbreak_final[]=$lb;
                    }
                    $now = (int)$row->X;
                    $add = count($listbreak_final);                   
                    
                    $total = $now + $add ;
                    
                    
                    return $total;                  
                    
                })
                ->edit('A',function($row){
                    $tahun_a =$this->request->getGet('tahun');
                    $bulan_a =$this->request->getGet('bulan');
                    $tahun_b =$this->request->getGet('tahunakhir');
                    $bulan_b =$this->request->getGet('bulanakhir');

                    $listbreakdate = $this->get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b);
                    $time_b=strtotime($tahun_b.'-'.$bulan_b.'-01');
                    $tanggal_b= date('Y-m-t',$time_b);
                    $listbreak_final=array();
                    foreach($listbreakdate as $lb){
                        if(date('Y-m-d',strtotime($lb)) <= date('Y-m-d'))
                        $listbreak_final[]=$lb;
                    }
                    $now = (int)$row->A;
                    $add = count($listbreak_final);                    
                    
                    $total = $now - $add ;
                    
                    
                    return $total;        
                })                  
                ->addNumbering() //it will return data output with numbering on first column
                // ->filter(function ($builder, $request) {        
                    // if ($request->tahun)
                        // $builder->where('Year(tanggal_absen)>=', $request->tahun);
                    // if ($request->bulan)
                        // $builder->where('month(tanggal_absen)>=', $request->bulan);  
                    // if ($request->tahunakhir)
                        // $builder->where('Year(tanggal_absen)<=', $request->tahunakhir);
                    // if ($request->bulanakhir)
                        // $builder->where('month(tanggal_absen)<=', $request->bulanakhir);  
                    // if ($request->kelas)
                        // $builder->where('laporan_absensi.kelas', $request->kelas);           
                // })
               ->toJson();
    }

    public function daftar_riwayat()
	{
        
		$data['title']='Riwayat Absensi Siswa ';
		return view('data_riwayat',$data);
    }
    public function daftar_riwayat_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('absensi')
                        ->select('id_absensi,kehadiran,nis,nama,nama_kelas')
                        ->join('siswa','absensi.siswa_id = siswa.id_siswa')
                        ->join('kelas','kelas.id_kelas = siswa.kelas ')
                        ->where('absensi.deleted_at IS NULL');  
						
                       
                         
			//print_r($builder->getCompiledSelect());  //print query

					
         
        return DataTable::of($builder) 
                ->hide('id_absensi')
                ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
			    
    }

    public function pesan_wa()
	{
        
        
        
        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();                
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);        
        $data['token']=$token;
		$data['title']='Kirim Pesan WhatsApp';
		return view('kirim_pesan',$data);
    }

    public function kirim_pesan_wa()
	{
        
        
        
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                 
                                'token' => 'required',                                
                                'nomor' => 'required|numeric',
                                'pesan' => 'required',
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');                
                    $value=array();
                    $value['id_kelas']=$this->request->getPost('ids');
                    $value['nama_kelas']=$this->request->getPost('nama');
                    $value['nis']=$this->request->getPost('nis');
                     
                    $m = new Mkelas();
                    $result = $m->save($value);
                    if($result){
                        if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                            $id=$m->getInsertID();
                            //echo 'valid';
                            $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                        }else{                         
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                        }                    
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/pesan');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/pesan');
    }

    public function setting(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Aplikasi';
        $s=new Msetting();
        $data['setting']=$s->first();
        return view('setting',$data);

        
   
    }
     public function setting_libur(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Hari Libur';
        $s=new Msetting();
        $data['setting']=$s->select('hari_libur')->first();
        return view('setting_libur',$data);        
   
    }
    public function setting_libur_save(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                                                        
                                    'token' => 'required',
                                     
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        $day = $this->request->getPost('day');
         
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    $value['id_setting']=1;                   
                    $day = $this->request->getPost('day');
                    
                    if(!isset($day[0])){
                        $day[0]=0;
                    }else{
                        $day[0]=1;
                    }
                    
                    if(!isset($day[1])){
                        $day[1]=0;
                    }else{
                        $day[1]=1;
                    }
                    
                    if(!isset($day[2])){
                        $day[2]=0;
                    }else{
                        $day[2]=1;
                    }

                    if(!isset($day[3])){
                        $day[3]=0;
                    }else{
                        $day[3]=1;
                    }

                    if(!isset($day[4])){
                        $day[4]=0;
                    }else{
                        $day[4]=1;
                    }

                    if(!isset($day[5])){
                        $day[5]=0;
                    }else{
                        $day[5]=1;
                    }

                    if(!isset($day[6])){
                        $day[6]=0;
                    }else{
                        $day[6]=1;
                    }
                    ksort($day);
                    $value['hari_libur']= implode(',',$day);
                    
                    
                    $m = new Msetting();
                    
                    $result = $m->save($value);
                    if($result){
                            
                                          
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting_libur');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting_libur');
    }
    public function setting_tanggal_save(){
        $session = session();        
        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'tanggal_libur' => 'required',
                                    'token' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    
                    $tanggal=explode('-',$this->request->getPost('tanggal_libur'));
                    $value['tanggal']=$tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
                    $db      = \Config\Database::connect();
                    $builder = $db->table('tanggal_libur');
                                       
                    $result = $builder->insert($value);
                    if($result){
                                                                   
                            $session->setFlashdata('error', 'Tanggal Berhasil Ditambah');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting_libur');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting_libur');
    }
    public function tanggal_libur_ajax()
    {
          
        $db = db_connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' );                        
                        
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                   return  '<button onclick="deleteTanggal(\''.$row->id_tanggal.'\')" class="btn btn-info btn-sm"  title="Hapus Tanggal"><i class="cil-x-circle"></i></button>';                                               
                    
                }, 'last')   
                         
                ->hide('id_tanggal')                
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }
   
    public function tanggal_libur_delete(){
        if( $this->request->getPost('id') !== NULL){        
            $session = session();   
            $db      = \Config\Database::connect(); 
            $builder = $db->table('tanggal_libur');
            $r=$builder->delete(['id_tanggal'=>$this->request->getPost('id')]);         
            if($r){
                $session->setFlashdata('pesan', 'Tanggal berhasil dihapus');
            }   else{
                $session->setFlashdata('pesan', 'Tanggal GAGAL dihapus');
            }
        }

            

        return redirect()->to('panel/setting_libur');
    }
    public function device(){
    
        $data['title']='Status Device WhatsApp';
        $wa = new Api_wa();
        $r=$wa->device();
        $x=$wa->view_qrcode();
         
        $data['device']=json_decode($r);
        $data['qr']=json_decode($x);
        return view('device',$data);

        
   
    }

    public function setting_save(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'masuk' => 'required',
                                    'limit' => 'required|numeric',
                                    'token' => 'required',
                                    'instansi' => 'required',
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    $value['id_setting']=1;
                    $value['nama_perusahaan']=$this->request->getPost('instansi');
                    $value['jam_masuk']=$this->request->getPost('masuk');
                    $value['batas']=$this->request->getPost('limit');
                    $value['wa_token']=$this->request->getPost('watoken');
                    $value['email']=$this->request->getPost('email');
                    $value['email_jam']=$this->request->getPost('email_jam');
                    $value['wa_jam']=$this->request->getPost('wa_jam');
                    
                    if($this->request->getPost('status') !==null)
                        $value['wa_status']=1;
                    else
                        $value['wa_status']=0;
                    
                    if($this->request->getPost('email_notif') !==null && $this->request->getPost('email')!=='')
                        $value['email_notif']=1;
                    else
                        $value['email_notif']=0;                    
                     
                    if($this->request->getPost('wa_notif') !==null)
                        $value['wa_notif']=1;
                    else
                        $value['wa_notif']=0;

                    $m = new Msetting();
                    
                    $result = $m->save($value);
                    if($result){
                            if( $value['wa_notif'] == 1)
                            $this->setting_reload();                  
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting');
    }

    public function setting_reload(){
        
        $wa = new Api_wa();
        $return = $wa->reload();         
        $rr = json_decode($return);
        $ms = new Msetting();
         
        
        
        if($rr->status== 'true'){
            
            $data =array();                
            $data['wa_exp']=$rr->device->expired;
            $data['id_setting']=1;
            $r=$wa->device();
            
            $r = json_decode($r);
            
             
            if($r->status == 'false'){            
                $data['wa_notif']=0;
            }
            
            $x= $ms->save($data);

        }else{
            
            $data['wa_status'] = 0;
            $data['wa_exp']='';
            $data['id_setting']=1;
            $data['wa_notif']=0;
            
            $ms->save($data);
        }
         return $return;
    }

function notifikasi_pendaftaran($no,$nama){
        $set = new Msetting();
        $setting = $set->first();
$text='*Sistem Absensi '.$setting['nama_perusahaan'].'*

nomor ini telah didaftarkan sebagai notifikasi absensi atas nama *'.$nama.'* 

_nb: jika ada kesalahan, segera hubungi kami_'; 

        $wa = new Api_wa();
        $r=$wa->send_wa($no,$text);
}

    /* WARNING DEVELOPER ONLY!! ADDING CALENDAR FOR PIVOTING */
    public function generate_calendar($year){


        $db = db_connect();
        $query = $db->query("select count(*) as jumlah from kalender where YEAR(tanggal) = ".$year);
        $r = $query->getResult();
        if($r[0]->jumlah == 0){
            // GENERATE DATE AND INSERT
            $begin = new \DateTime( $year.'-01-01' );
            $end = new \DateTime( $year.'-12-31 +1day'  );        
            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);        
            $t=array();
            $tanggal=array();
            foreach($daterange as $date){
                $tanggal[]=array('tanggal'=>$date->format("Y-m-d"));

            }      

            $db      = db_connect();
            $builder = $db->table('kalender');
            $result = $builder->insertBatch($tanggal);
            //$result = $builder->affectedRows();
            echo $result.' untuk tahun '.$year.' data telah ditambahkan';

        }else{
            echo 'TAHUN SUDAH DITAMBAHKAN';
        }
         


    }

    public function notifikasi(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Aplikasi';
        $s=new Mnotifikasi();
         
        $data['notifikasi']=$s->findAll();

         
        return view('setting_notifikasi',$data);

        
   
    }
    public function notifikasi_save(){
    
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'text' => 'required',                                     
                                    'token' => 'required'                                    
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                $text = $this->request->getPost('text');
                $status = $this->request->getPost('status_text');
                $vall = array();
                $m = new Mnotifikasi();

                foreach ($text as $key => $value) {
                    if(!empty($status[$key]))
                        echo 'key '.$key.', text '.$value.', status 1 <br>';
                    else
                        echo 'key '.$key.', text '.$value.', status 0 <br>';
                    $data = array();
                    if(!empty($status[$key]))
                        $data=array('id_no'=>$key,'text'=>$value,'status'=>1);
                    else 
                        $data=array('id_no'=>$key,'text'=>$value,'status'=>0);
                        $result = $m->save($data);
                    
                }
                 
                
                    
                    
                    if($result){                            
                            
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/notifikasi');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/notifikasi');

        
   
    }
    public function percobaan(){
        $request = $this->request->getPost();
        $laporan =array(2,4,5,6,7);
        $result =array();
        if(in_array($request['id'],$laporan)){
            $s =new Msetting();
            $setting = $s->first();
            $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                            'nama' =>'Mas Arwin',
                            'telp_instansi'=>'0',
                            'kelas'=>'X',
                            'nis'=>'20019283');
            $status = $this->m_pesan($request['id'],$arrdata);
           
            if($status['status'] == 1){
                $wa = new Api_wa();
                $r=$wa->send_wa($request['no'],$status['pesan']);
                
                $rx = json_decode($r);
                
                if($rx->status=='true'){
                    $result['status']=1;
                    $result['pesan']='Pesan Terkirim';        
                }else{
                    $result['status']=0;
                    $result['pesan']='Pesan Tidak Dapat Terkirim Cek API WA';        
                }
            }else{
                    $result['status']=0;
                    $result['pesan']=$rx->status;   
            }             
        }else if(in_array($request['id'],array(1,3) )){
            $result['status']=0;
            $result['pesan']='fitur belum tersedia';
            
        }else{
            $result['status']=0;
            $result['pesan']='Pesan tidak ditemukan';
        }
        echo json_encode($result);
    }

    public function m_pesan($idpesan,$arrdata){

        $result = array();
            if($idpesan > 0 && $idpesan < 8){
                $n = new Mnotifikasi();
                $m = $n->find($idpesan);

                if($m!=''){
                    $pesan = $m['text'];
                    
                   
                    if(!empty($arrdata['instansi'])){
                        $pesan=str_replace('[INSTANSI]',$arrdata['instansi'],$pesan);       
                                         
                    }
                    
                    if(!empty($arrdata['telp_instansi'])){
                        $pesan=str_replace('[TELP_INSTANSI]',$arrdata['telp_instansi'],$pesan);                        
                    }
                    if(!empty($arrdata['nama'])){
                        $pesan=str_replace('[NAMA]',$arrdata['nama'],$pesan);                        
                    }
                    if(!empty($arrdata['kelas'])){
                        $pesan=str_replace('[KELAS]',$arrdata['kelas'],$pesan);                        
                    }
                    if(!empty($arrdata['nis'])){
                        $pesan=str_replace('[NIS]',$arrdata['nis'],$pesan);                        
                    }
                    
                    $pesan=str_replace('[DATE]',date('d-m-Y'),$pesan);                        
                    
                    $result['status']=1;
                    $result['pesan']=$pesan;                     
                    $result['enable']=$m['status'];                     

                }else{
                    $result['status']=0;
                    $result['pesan']='Pesan Tidak Ditemukan';    
                    $result['enable']=0;                     
                }
            }else{
                $result['status']=0;
                $result['pesan']='id pesan tidak sesuai';
                $result['enable']=0;

            }

            return $result;
    }

    /* USER ADMIN */
    public function user_admin(){

        
        $data['title']= 'Daftar User Admin';
        return view('data_users',$data);
    }

    public function user_admin_ajax(){
        $db = db_connect();
        $builder = $db->table('users')
                        ->select('uid,nama,username,email,password,akses')
                        ->where('users.deleted_at IS NULL');
         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    $text='';
                    
                    if(session()->get('akses') == '1' || $row->uid == session()->get('uid'))
                    $text = '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->uid.'" data-nama="'.$row->nama.'" data-username="'.$row->username.'" data-email="'.$row->email.'" data-akses="'.$row->akses.'"   title="Edit User"><i class="cil-pencil"></i></a>';

                    if($row->uid == 1 || $row->uid == session()->get('uid') ){

                    }else
                     {
                         if(session()->get('akses') == 1)
                            $text .='<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->uid.'" title="Hapus User"><i class="cil-trash"></i></a>';
                     }   
                    if(session()->get('akses') == '1' || $row->uid == session()->get('uid'))											   
                        $text .='<button class="btn btn-transparent btn-sm text-info" title="Lihat Password" onclick="alert(\''.$row->password.'\')"><i class="cil-low-vision"></i></button>';
                    return $text;
                }, 'last')
                 
                ->edit('akses', function($row){
                    if($row->akses==1)
                        return 'Admin';                    
                    else
                        return 'Operator';
                })
               ->hide('uid')
               ->hide('password')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function tes(){
        print_r(session()->get());
        print_r(session()->get('akses'));
    }

    public function admin_username_check(){
        if( $this->request->getPost('username') !== NULL){
			$con=array();
            $con['username']=$this->request->getPost('username');
            $u=new Musers();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }

    public function admin_user_add(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nama' => 'required',
                                'username' => 'required',                                
                                'akses' => 'numeric'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $con=array();
            $con['uid']=$this->request->getPost('id');
            $con['nama']=$this->request->getPost('nama');
            $con['username']=$this->request->getPost('username');
            if($this->request->getPost('id') == '')
                $con['password']=$this->request->getPost('password');
            else if($this->request->getPost('password')!='')
                $con['password']=$this->request->getPost('password');
            $con['email']=$this->request->getPost('email');
            $con['akses']=$this->request->getPost('akses');
             
            $u= new Musers();
            $result= $u->save($con);
                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$u->getInsertID();
                        //echo 'valid';
                        $session->setFlashdata('error', 'Data berhasil disimpan');         
                    }else{                         
                         $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                    }                    
                }else{
                    $session->setFlashdata('error', 'Update Data GAGAL');  
                }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Admin GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/users');
    }
    public function admin_user_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Musers();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('pesan', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'user GAGAL dihapus');
			}
		}

			
        
		return redirect()->to('panel/users');
		 
    }


    public function riwayatwa()
	{
        $kelas= new Mkelas();
        $data['kelas']= $kelas->findAll();
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Riwayat Pesan WA';
		return view('data_log_wa',$data);
    }
    public function riwayatwa_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('log_wa')
                        ->select('id,no,text,status,keterangan,created_at');
                         
                         

         
        return DataTable::of($builder)                                       
                ->hide('id')                
                ->addNumbering() //it will return data output with numbering on first column                
               ->toJson();
    }

    public function cek_siswa()
	{
         
        $data['title']='Cek Data Siswa';        

		return view('cek_data_siswa',$data);
    }

    public function cek_data_siswa(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nis' => 'required'                                 
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
                $nis= $this->request->getPost('nis');                 

                $m = new Msiswa();
                        $siswa = $m->where(array('nis'=>$nis))->first();
                         
                        if($siswa == ''){
                            $result['status'] = 0;
                            $result['text'] = 'Gagal! Data Tidak Ditemukan';
                        }else{
                            
                                    $result['status']=1;
                                    $result['siswa']=$siswa;
                                    $result['text'] = 'Data Ditemukan';
                                
                            }
        }
        else{
            $errors = $validation->getErrors();
            $result['status']=0;            
            $result['text'] =json_encode($errors);            
        }

        header('Content-Type: application/json');		
		echo  json_encode($result);
    }

}