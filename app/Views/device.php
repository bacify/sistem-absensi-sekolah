<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>                
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12">
                    
                   
                    <?php  
                    
                        if($device->status == 'true'){
                            
                            echo '<p class="text-info">Status Terhubung :'.$device->pesan.' </p>';
                            echo '<p>Nomor Terhubung :'.$device->phoneNumber.' </p>';
                            echo '<p>Nama :'.$device->name.' </p>';
                            echo '<p>Os :'.$device->os_version.' </p>';
                            echo '<p>Brand :'.$device->manufacturer .' </p>';
                            echo '<p>Model :'.$device->model .' </p>';
                        // print_r($device);
                        }else{
                            echo '<p class="text-warning ">'.$device->pesan.'</p>';
                            echo '<strong>WhatsApp Belum Terhubung, silahkan scan QRCODE untuk mengghubungkan</strong>';
                            
                        }
                    ?>
                    
                    </div> 
                    <div class="col-lg-4 col-md-12 col-sm-12 text-center">
                   
                    <?php 
                    
                    if($qr->status=='1'){
                       
                        echo '<img src="'.$qr->qrcode.'" class="d-block mx-auto">';
                        echo '<strong> Scan barcode untuk menghubungkan WA</strong>';
                    }
                    ?>
                    
                    </div>                
                </div>
            </div>
          </div>
 
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
 
<script type="text/javascript">

function autoRefreshPage()

{

    window.location = window.location.href;

}

setInterval('autoRefreshPage()', 30000);

</script>
<?= $this->endSection() ?>
