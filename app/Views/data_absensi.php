<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-inline mb-2">
                    <div class="form-group">
                    <label for="tanggal" class="mb-0 pb-0 mr-2">Tanggal :</label> 
                                <select name="tanggal" id="filter_tanggal" class="filtertanggal form-control" title="Pilih Kelas" >                                 
                                    <?php 
                                        foreach ($tanggal as $k) {
                                            echo '<option value="'.$k['input'].'" > '.$k['value'].' </option>';
                                        }
                                    ?>
                                </select>
                    </div>
                    <div class="form-group mx-3">
                    <label for="kelas" class="mb-0 pb-0 mr-2">Kelas :</label> 
                                <select name="kelas" id="filter_kelas" class="filterkelas form-control" title="Pilih Kelas" >                                 
                                    <?php 
                                        $x=1;
                                        foreach ($kelas as $k) {
                                            if($x==1)
                                                echo '<option value="'.$k['id_kelas'].'" selected> '.$k['nama_kelas'].' </option>';
                                            else 
                                                echo '<option value="'.$k['id_kelas'].'"> '.$k['nama_kelas'].' </option>';

                                                $x++;
                                        }
                                    ?>
                                </select>
                    </div>
                </div>
                <div class="table-responsive">
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>Nama Siswa</th>
                        <th>NIS</th>
                        <th>Kelas</th>
                        <th>Tanggal Absen</th>
                        <th>keterangan</th>
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                     
                    </table>
                </div>
                </div>
                 
              </div>
            </div>
          </div>

<!-- Modal Add Product-->
<form id="form-tambah-siswa" action="<?php echo base_url('panel/a/absensi_manual');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Absensi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Siswa</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="id_absen" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                            <input type="hidden" name="created_by" value="manual">
                           <input type="text" name="nama" class="form-control" placeholder="Nama Siswa" autocomplete="off" readonly>                            
                       </div>
                        <div class="form-group">
                            <label for="nis" class="mb-0 pb-0">NIS</label>                                  
                            <input type="text" id="nis" name="nis" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="kelas" class="mb-0 pb-0">Kelas</label>
                            <select id="kelas-add" name="kelas" class="form-control" title="Pilih Kelas" readonly> 
                                <?php 
                                    foreach ($kelas as $k) {
                                        echo '<option value="'.$k['id_kelas'].'" > '.$k['nama_kelas'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                        
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">keterangan</label>       
                            <select name="keterangan"  class="form-control" title="Pilih Keterangan" required> 
                                <option class="text-success font-weight-bold" value="M">Masuk</option>
                                <option class="text-primary font-weight-bold" value="I">Ijin</option>
                                <option class="text-danger font-weight-bold" value="A">Alpha</option>
                                <option class="text-info font-weight-bold" value="S">Sakit</option>                                
                            </select>
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/a/absensi_manual');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Siswa</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="id_absen" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                            <input type="hidden" name="created_by" value="manual">
                           <input type="text" name="nama" class="form-control" placeholder="Nama Siswa" autocomplete="off" readonly>                            
                       </div>
                        <div class="form-group">
                            <label for="nis" class="mb-0 pb-0">NIS</label>                                  
                            <input type="text" id="nis" name="nis" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="kelas" class="mb-0 pb-0">Kelas</label>
                            <select id="kelas-add" name="kelas" class="form-control" title="Pilih Kelas" readonly> 
                                <?php 
                                    foreach ($kelas as $k) {
                                        echo '<option value="'.$k['id_kelas'].'" > '.$k['nama_kelas'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                        
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">keterangan</label>       
                            <select name="keterangan"  class="form-control" title="Pilih Keterangan" required> 
                                <option  value="M">Masuk</option>
                                <option  value="I">Ijin</option>
                                <option  value="A">Alpha</option>                                
                                <option  value="S">Sakit</option>                                
                            </select>
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/siswa_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

        
        
       
        
        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("nis"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



        // check ID
        $('input[name=nis]').change(function(){
           $('.nisid').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconnisid').show();
            $('.nisid-gagal').hide();
            $('.nisid-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/d/check_nis');?>",
                data: {nis: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.nisid').addClass('text-success');
                            $('.loadingiconnisid').hide(); 
                            $('.nisid-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.nisid').addClass('text-danger');
                            $('.loadingiconnisid').hide(); 
                            $('.nisid-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });

        function adduser(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnisid').hide();
            $('.nisid-gagal').hide();
            $('.nisid-ok').hide();
            $('#form-tambah-siswa').trigger("reset");
            
        }


 $(document).ready(function() {
      
    $('select').selectpicker();
    $('.filtertanggal').selectpicker('val','<?=date("Y-m-d");?>');
    

    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 "infoFiltered": "",
                 "infoPostFix": ""
                },
            processing: true,
            serverSide: true,
            "pageLength": 50,
            // responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/absensi_ajax')?>",
                data:function(d){
                    d.kelas = $('.filterkelas').selectpicker('val');
                    d.tanggal = $('.filtertanggal').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                { targets: -2, className: 'text-center'}, //last column center.
                { targets: [0,-1,-2], searchable: false}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-success',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
    });

    $('#filter_kelas').change(function(event){
        
        console.log('kelas');
        table.ajax.reload();
    })
    $('#filter_tanggal').change(function(event){
        console.log('filtertanggal');
        table.ajax.reload();
    })

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');

    $('#tabel-utama').on('click','.add_record',function(){
            let id=$(this).data('id');
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/d/siswa_detail');?>/"+id, 
                dataType: "json",               
                }).done(function(msg ) {
                   // console.log(msg);
                    $('#form-tambah-siswa [name="ids"]').val(id);
                    $('#form-tambah-siswa [name="nis"]').val(msg.result.nis);
                    $('#form-tambah-siswa [name="nama"]').val(msg.result.nama);
                    $('#form-tambah-siswa [name="kelas"]').selectpicker('val',msg.result.kelas);   
                    $('#form-tambah-siswa [name="kelas"]').prop('disabled', true);   
                    $('#form-tambah-siswa [name="kelas"]').selectpicker('refresh');         
                                   
                    $('#myModalAdd').modal('show')
                        
                
                });
            
            
              
    });
    $( "#form-tambah-siswa" ).submit(function( event ) {
        $('#myModalAdd').modal('hide');         
        event.preventDefault();
        let id= $('#form-tambah-siswa [name="ids"]').val();        
        let created= $('#form-tambah-siswa [name="created_by"]').val();
        let keterangan= $('#form-tambah-siswa [name="keterangan"]').val();
        let id_absen= $('#form-tambah-siswa [name="id_absen"]').val();
        let tanggal=$('.filtertanggal').selectpicker('val');
        $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/a/absensi_manual_ajax');?>",
                data: { ids: id,                        
                        created_by:created,
                        keterangan:keterangan,
                        id_absen:id_absen,
                        tanggal_absen:tanggal
                        }
                }).done(function(r) {
                    let hasil = JSON.parse(r);
                    
                    table.ajax.reload();
                    alert(hasil.pesan);
                    
                })
                .fail(function(r) {
                    let hasil = JSON.parse(r);
                    alert(hasil.pesan);
                    
                })
                .always(function() {
                    //alert( "complete" );
                });
    });
    // get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            let id=$(this).data('id');
            let absen=$(this).data('idabsen');
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/d/siswa_detail');?>/"+id, 
                dataType: "json",               
                }).done(function(msg ) {
                     
                    $('#updateform [name="ids"]').val(id);
                    $('#updateform [name="id_absen"]').val(absen);
                    $('#updateform [name="nis"]').val(msg.result.nis);
                    $('#updateform [name="nama"]').val(msg.result.nama);
                    $('#updateform [name="kelas"]').selectpicker('val',msg.result.kelas);   
                    $('#updateform [name="kelas"]').prop('disabled', true);   
                    $('#updateform [name="kelas"]').selectpicker('refresh');         
                                   
                    $('#ModalUpdate').modal('show')
                        
                
                });
             
              
    });


    $( "#updateform" ).submit(function( event ) {
        $('#ModalUpdate').modal('hide');         
        event.preventDefault();
        let id= $('#updateform [name="ids"]').val();
        
        let created= $('#updateform [name="created_by"]').val();
        let keterangan= $('#updateform [name="keterangan"]').val();
        let id_absen= $('#updateform [name="id_absen"]').val();
        $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/a/absensi_manual_ajax');?>",
                data: { ids: id,                        
                        created_by:created,
                        keterangan:keterangan,
                        id_absen:id_absen
                        }
                }).done(function(r) {
                    let hasil = JSON.parse(r);
                    
                    table.ajax.reload();
                    alert(hasil.pesan);
                    
                })
                .fail(function(r) {
                    let hasil = JSON.parse(r);
                    alert(hasil.pesan);
                    
                })
                .always(function() {
                    //alert( "complete" );
                });
    });

    $('#tabel-utama').on('click','.delete_record',function(){
                var id=$(this).data('id');
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id"]').val(id);
    });


    
});
</script>
<?= $this->endSection() ?>
