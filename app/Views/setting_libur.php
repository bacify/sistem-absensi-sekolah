<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                 
            </div>
            <form id="form-pengaturan" action="<?php echo base_url('panel/setting_libur/save');?>" method="post">
            <div class="card-body">
                 
                    <?php  $hari = explode(',',$setting['hari_libur']);?>
                     <div class="form-group row">
                         <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Senin</label>    
                        </div>
                        <div class="col">
                        <input type="hidden" name="token" value="<?=$token;?>">
                            <input type="checkbox" <?php if($hari[0]==1) echo 'checked';?> name="day[0]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div>
                     </div> 
                     <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Selasa</label>    
                        </div>
                        <div class="col">
                            <input type="checkbox" <?php if($hari[1]==1) echo 'checked';?> name="day[1]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div>                      
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Rabu</label>  
                        </div>
                        <div class="col">  
                            <input type="checkbox" <?php if($hari[2]==1) echo 'checked';?> name="day[2]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div>
                     </div> 
                     <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Kamis</label>    
                        </div>
                        <div class="col">
                            <input type="checkbox" <?php if($hari[3]==1) echo 'checked';?> name="day[3]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div> 
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Jumat</label>    
                        </div>
                        <div class="col">
                            <input type="checkbox" <?php if($hari[4]==1) echo 'checked';?> name="day[4]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div> 
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Sabtu</label>    
                        </div>
                        <div class="col">
                            <input type="checkbox" <?php if($hari[5]==1) echo 'checked';?> name="day[5]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div> 
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 col-sm-6">
                            <label for="limit" class="mb-0 pb-0">Minggu</label>    
                        </div>
                        <div class="col">
                            <input type="checkbox" <?php if($hari[6]==1) echo 'checked';?> name="day[6]" data-toggle="toggle" data-on="Libur" data-off="Masuk" data-onstyle="danger" data-offstyle="success">
                        </div> 
                    </div>
             
            </div>
            <div class="card-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary tombolsubmit float-right mb-1">Simpan</button>
            </div>
            </form>
        </div>

        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left">Tanggal Libur</h3>
                 
            </div>
             <div class="card-body">
                 
             <form id="form-pengaturan-tanggal" action="<?php echo base_url('panel/setting_tanggal_libur/save');?>" method="post">
                <div class="form-group row">
                <input type="hidden" name="token" value="<?=$token;?>">
                    <div class="col-6">                                
                            <div class="input-group date" id="tanggal-libur" data-target-input="nearest">                           
                                <input type="text" name="tanggal_libur" class="form-control datetimepicker-input" data-target="#tanggal-libur" value="" required>
                               
                                <div class="input-group-append" data-target="#tanggal-libur" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                    </div>
                    <div class="col-6">                                  
                            <button type="submit" name="submit" class="btn btn-primary ">Tambah tanggal</button>
                    </div>
                </div> 
            </form>
                <div class="form-group">
                     <div class="table-responsive">
                    <table id="tabel-utama" class="table table-striped table-bordered datatable">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Tanggal</th>                             
                            <th>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
            </div>
             
        </div>


          <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/tanggal_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Tanggal</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>

<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>
function deleteTanggal(id){
    
                
    $('#ModalDelete').modal('show');
    $('#deleteform [name="id"]').val(id);

}
function reloadsetting(btn){
     btn.disabled = true;
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/setting/reload');?>",
                dataType: "json",
                }).done(function(msg ) {
                    
                       if(msg.status){
                           alert(msg.pesan);
                           location.reload();
                       }else{
                            alert(msg.pesan);
                            btn.disabled = false;
                       }
                
                });
}
$(document).ready(function() {
    
    $('#tanggal-libur').datetimepicker({
                format: 'DD-MM-YYYY',                
                });  

    let table = $('#tabel-utama').DataTable({ 
                "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 "infoFiltered": "",
                 "infoPostFix": ""
                },
            processing: true,
            serverSide: true,
            "pageLength": 50,
            // responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/d/tanggal_libur')?>",
                   },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                                
            ],
            "dom": 'ti',
          
    });



    

});
</script>
<?= $this->endSection() ?>