<<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button  class="btn btn-sm btn-outline-primary float-right my-1" data-toggle="modal" data-target="#modalcek" > <i class="cil-send"> Tes Pesan</i> </a>
            </div>
            <form id="form-pengaturan" action="<?php echo base_url('panel/notifikasi/save');?>" method="post">
            <div class="card-body">               
                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Email Pendaftaran</label>                       
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <textarea id="text_1" class="form-control" name="text[1]"  rows="4"></textarea>                            
                            <small id="passwordHelpBlock" class="form-text text-info">
                                    *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                        
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi email Pendaftaran</label>    
                            <div class="d-block"   >
                                <input id="status_1" name="status_text[1]" type="checkbox" value="1" <?php if($notifikasi[0]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>  
                    <hr>


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Wa Pendaftaran</label>                                             
                        <textarea id="text_2" class="form-control" name="text[2]" rows="4"></textarea>                            
                            <small id="passwordHelpBlock" class="form-text text-info">
                            *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                        
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Wa Pendaftaran</label>    
                            <div class="d-block"   >
                                <input id="status_2" name="status_text[2]" type="checkbox" value="1" <?php if($notifikasi[1]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>  
                    <hr>


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Laporan Email</label>                       
                         
                        <textarea id="text_3" class="form-control" name="text[3]" rows="4"></textarea>                         
                        <small id="passwordHelpBlock" class="form-text text-info">
                        *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                       
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Laporan Email</label>    
                            <div class="d-block"   >
                                <input id="status_3" name="status_text[3]" type="checkbox" value="1" <?php if($notifikasi[2]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>
                    <hr> 


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Wa Absensi <span class="badge badge-success">Masuk</span></label>                       
                         
                        <textarea id="text_4" class="form-control" name="text[4]" rows="4"></textarea>                       
                        <small id="passwordHelpBlock" class="form-text text-info">
                        *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                       
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Wa Absensi <span class="badge badge-success">Masuk</span></label>    
                            <div class="d-block">
                                <input id="status_4" name="status_text[4]" type="checkbox" value="1" <?php if($notifikasi[3]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div> 
                    <hr>


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Wa Absensi <span class="badge badge-danger">Alpha</span></label>
                       
                         
                        <textarea id="text_5" class="form-control" name="text[5]" rows="4"></textarea>                            
                        <small id="passwordHelpBlock" class="form-text text-info">
                        *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                        
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Wa Absensi <span class="badge badge-danger">Alpha</span></label>    
                            <div class="d-block">
                                <input id="status_5" name="status_text[5]" type="checkbox" value="1" <?php if($notifikasi[4]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div> 
                    <hr>


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Wa Absensi <span class="badge badge-primary">Ijin</span></label>
                                                 
                        <textarea id="text_6" class="form-control" name="text[6]" rows="4"></textarea>                        
                        <small id="passwordHelpBlock" class="form-text text-info">
                        *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                        
                    </div>
                     
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Wa Absensi <span class="badge badge-primary">Ijin</span></label>    
                            <div class="d-block">
                                <input id="status_6" name="status_text[6]" type="checkbox" value="1" <?php if($notifikasi[5]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div> 
                    <hr>


                    <div class="form-group">
                        <label for="nama" class="mb-0 pb-0">Notifikasi Wa Absensi <span class="badge badge-info">Sakit</span></label>
                                                 
                        <textarea id="text_7" class="form-control" name="text[7]" rows="4"></textarea>                           
                        <small id="passwordHelpBlock" class="form-text text-info">
                        *referensi <b>[INSTANSI]</b> = Nama Instansi, <b>[TELP_INSTANSI]</b> = telp Instansi, <b>[NAMA]</b> = nama siswa, <b>[KELAS]</b> = kelas siswa, <b>[NIS]</b> = nis siswa , <b>[DATE]</b> = tanggal hari ini, *text* = text tebal, _text_ = text miring, ~text~ = text coret
                            </small>                        
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Wa Absensi <span class="badge badge-info">Sakit</span></label>    
                            <div class="d-block">
                                <input id="status_7" name="status_text[7]" type="checkbox" value="1" <?php if($notifikasi[6]['status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>  
                     
            </div>
            
            <div class="card-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit float-right mb-1">Simpan</button>
            </div>
            </form>
        </div>





        <!-- modal testing -->
        <div class="modal fade " id="modalcek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Testing Pesan</h4>
                  
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                  
                        <div class="form-group">
                            <label for="kategori" class="mb-0 pb-0">Jenis Pesan</label>
                            <select id="idpesan" name="jenis" class="form-control" data-live-search="true" title="Jenis Katalog" required> 
                                <option value="1">EMAIL PENDAFTARAN</option>
                                <option value="2">WA PENDAFTARAN</option>
                                <option value="3">EMAIL ABSENSI</option>
                                <option value="4">WA ABSENSI MASUK</option>
                                <option value="5">WA ABSENSI ALPHA</option>
                                <option value="6">WA ABSENSI IJIN</option>
                                <option value="7">WA ABSENSI SAKIT</option>
                                                 </select>
                       </div>
                        <div class="form-group">
                        <label for="nowa" class="mb-0 pb-0">No WA Tujuan (untuk testing)</label>                            
                           <input id="nowa" type="number" name="no" class="form-control" placeholder="085730333222">
                       </div>
                       <center><b>*PASTIKAN PESAN TELAH DISIMPAN DAHULU SEBELUM DICOBA*</b></center>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="tomboltes" type="button" name="submit" class="btn btn-success">Jalankan</button>
                   </div>
                    </div>
            </div>
         </div>

<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script>
 
$(document).ready(function() {
    
    <?php 
    
    foreach($notifikasi as $not){ 
        
        echo "$('#text_".$not['id_no']."').val('".str_replace(array("\r\n"), '\n',$not['text'])."');";

    }
    ?>


        $('#tomboltes').click(function(){
            $('#tomboltes').prop( "disabled", true );
            let idpesan = $('#idpesan').val();
            let wa = $('#nowa').val();

            if(wa == '')
            {
                alert ('Silahkan isi no WA!');
                return
            }
            console.log('pesan '+idpesan+', wa '+wa);
             
                
                $.ajax({
                dataType: "json",
                type: "POST",
                url: '<?php echo base_url('panel/tespesan');?>',
                data:{id : idpesan, no :wa }
                }).done(function(data) {    
                    if(data['status']==1)
                        alert('Sukses! '+data['pesan']);
                    else
                        alert('Gagal! '+data['pesan']);
                }).fail(function(data){
                    console.log(data);
                }).always(function() {
                    $('#tomboltes').prop( "disabled", false );
                });;
                
        })
    

});

 

</script>
<?= $this->endSection() ?>