<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button class="btn btn-success float-right" onclick="adduser()">Tambah</button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                    
                    
                    </div>                 
                </div>
            </div>
          </div>
 
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
 
 
<script>
 
</script>
<?= $this->endSection() ?>
