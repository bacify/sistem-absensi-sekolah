<?php

namespace App\Models;

use CodeIgniter\Model;

class Msiswa extends Model
{
    protected $table      = 'siswa';
    protected $primaryKey = 'id_siswa';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_siswa','nama','nis','alamat','kelas','no_telp','notifikasi'];
}