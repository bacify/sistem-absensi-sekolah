<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkelas extends Model
{
    protected $table      = 'kelas';
    protected $primaryKey = 'id_kelas';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_kelas','nama_kelas'];
}