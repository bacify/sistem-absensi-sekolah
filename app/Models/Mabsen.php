<?php

namespace App\Models;

use CodeIgniter\Model;

class Mabsen extends Model
{
    protected $table      = 'absensi';
    protected $primaryKey = 'id_absensi';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_absensi','siswa_id','kehadiran','keterangan','user_id','created_by'];
}