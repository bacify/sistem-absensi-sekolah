<?php

namespace App\Models;

use CodeIgniter\Model;

class Msetting extends Model
{
    protected $table      = 'setting';
    protected $primaryKey = 'id_setting';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_setting','nama_perusahaan','jam_masuk','batas','hari_libur','wa_token','wa_status','wa_exp','email','wa_notif','email_notif','wa_jam','email_jam'];
}