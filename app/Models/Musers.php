<?php

namespace App\Models;

use CodeIgniter\Model;

class Musers extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'uid';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['uid','username','password','nama','email','akses'];
}