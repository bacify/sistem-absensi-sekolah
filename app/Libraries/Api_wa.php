<?php

namespace App\Libraries;
use App\Models\Msetting; 
 

class Api_wa  
{
   // public $token = 'JRuUNNpWA1LgkbwRTLYxCaRQWKpIhtVf4Hxy3jfnQ1142RXDGf';
    public $token = '';
    public $url = 'https://app.ruangwa.id/api';

    function __construct() {
        $ms = new Msetting();
        $r = $ms->first();
        $this->token = $r['wa_token'];
      }

    public function reload(){
        $r = $this->info('bacify');         
        $r= json_decode($r);
        
        if($r->status== 'true'){
            if(count($r->devices)>0){
                $exp = $r->devices[0]->expired;                 
            }
            
            
            return json_encode(array('status'=>'true', 'pesan'=> $r->pesan, 'device'=> $r->devices[0])); 
        }
        else{
            
            return json_encode(array('status'=>'false', 'pesan'=> $r->pesan));   
        }
    }
    // Fungsi ini untuk melihat dan scan QR Code dan menghubungkan ke whatsapp. Nilai yang dikeluarkan berupa QR Code base64.
	public function view_qrcode()
	{          
		$token = $this->token;
        
        $url = $this->url.'/qrcode';

        
         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token                                                                                                               
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
            
            $r = json_decode($body);
            
            if(isset($r->status) && $r->status == 1){
                if($r->message == 'AUTHENTICATED')
                    return json_encode(array('status'=>0, 'pesan'=> $r->message,'qrcode'=>''));   
                else {
                    return json_encode(array('status'=>1, 'pesan'=> $r->message,'qrcode'=>$r->qrcode));   
                }
            }                
            else
                return json_encode(array('status'=>0, 'pesan'=> $r->message,'qrcode'=>''));   
        }else{
            return json_encode(array('status'=>0, 'pesan'=> 'Error','qrcode'=>''));   
        }
     
        

    }
     // Fungsi ini untuk melihat dan scan QR Code dan menghubungkan ke whatsapp. Nilai yang dikeluarkan berupa QR Code base64.
	public function view_qrcode_image()
	{          
		$token = $this->token;
        
        $url = $this->url.'/qrcode_image';

        
         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token                                                                                                               
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
            return $body;
            // $r = json_decode($body);
            //  print_r($r);
            // if($r->result== true)
            //     return json_encode(array('status'=>$r->result, 'pesan'=> $r->message,'qrcode'=>$r->qrcode));   
            // else
            //     return json_encode(array('status'=>$r->result, 'pesan'=> $r->message,'qrcode'=>''));   
        }else{
           // return json_encode(array('status'=>false, 'pesan'=> 'Error','qrcode'=>''));   
           return '';
        }
     
        

    }
        
    // Fungsi ini berisi informasi akun dan device. Misal: nama, saldo, token, tanggal expired device dan seterusnya.
	public function info($username)
	{          
		$token = $this->token;        
        $url =  $this->url.'/info';
        $user = $username;
         
         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token,                                                                                                               
                                            'username' => $user                                                                                                               
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){             
            $r = json_decode($body);     
            
            if($r->result== 'true'){
                
                return json_encode(array('status'=>$r->result, 'pesan'=> $r->message, 'nama'=> $r->nama, 'balance'=> $r->balance, 'devices'=> $r->devices));   
            }   
            else
                     return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));      
        }else{
            return json_encode(array('status'=>false, 'pesan'=> 'Error'));       
        }        

    }
    // Fungsi ini digunakan untuk cek nomor apakah sudah terdaftar di Whatsapp atau belum.
	public function ceknomor($no)
	{          
		$token = $this->token;
        
        $url = $this->url.'/check_number';
        $number = $no;
         
         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token,                                                                                                               
                                            'number' => $number                                                                                                               
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
             
            $r = json_decode($body);
           // print_r($r);
            if($r->result== true)
                if($r->onwhatsapp == true)
                    return json_encode(array('status'=>$r->onwhatsapp, 'pesan'=> $r->message));                       
                else
                    return json_encode(array('status'=>$r->onwhatsapp, 'pesan'=> $r->message));      
            else
            return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));      
        }else{
            return json_encode(array('status'=>false, 'pesan'=> 'Error'));       
        }
     
        

    }
    // Permintaan ini berfungsi untuk mengirim pesan teks ke WhatsApp dengan lebih cepat dari pada send_message 
	public function send_wa($no,$msg)
	{          
		$token = $this->token;
        $phone = $no; //untuk group pakai groupid contoh: 62812xxxxxx-xxxxx
        $message = $msg;
        $url =  $this->url.'/send_express';

         

         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token,
                                            'number' => $phone,
                                            'message' => $message,                                                                     
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
             
            $r = json_decode($body);            
            if($r->result == 'true')            
                return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));       
            else
                return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));      
        }else{
            return json_encode(array('status'=>'false', 'pesan'=> 'Error'));        
        }
     
        

    }
	public function send_button($no)
	{          
		$token = $this->token;
        $phone = $no; //untuk group pakai groupid contoh: 62812xxxxxx-xxxxx
        $contenttext= "Testing kirim button";
        $footertext= "by ruangwa";
        $buttonid= "id1";
        $buttontext= "OK";
        $url =  $this->url.'/send_button';

         

         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token,
                                            'number' => $phone,
                                            'contenttext'   => $contenttext,
                                            'footertext'   => $footertext,
                                            'buttonid'   => $buttonid,
                                            'buttontext'   => $buttontext                                                                    
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
             
            $r = json_decode($body);
            
            if(!$r->result)            
            return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));       
            else
                return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));      
        }else{
            return json_encode(array('status'=>false, 'pesan'=> 'Error'));        
        }
     
        

    
    }
	 
    // Fungsi ini untuk melihat status device seperti status koneksi, nomor, alias, brand dan model device, platform, battery dan plugged.
	public function device()
	{          
		$token = $this->token;
         
        $url =  $this->url.'/device';

         

         
        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', 
                                     $url,                                      
                                     [  'connect_timeout' => 30,
                                        'form_params' => [
                                            'token' => $token                                                                                                             
                                        ],
                                        'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded'
                                        ]
                                    ]);
        $code   = $response->getStatusCode(); // 200
        $reason = $response->getReason(); // OK
        $body = $response->getBody();

        if($code == 200){
                         
            $r = json_decode($body);
          //  print_r($r);
            if($r->result == 'true')            
                 return json_encode(array('status'=>$r->result, 'pesan'=> $r->message, 'phoneNumber'=> $r->phoneNumber, 'id'=> $r->id, 'name'=> $r->name,'os_version'=> $r->os_version,'manufacturer'=> $r->manufacturer,'model'=> $r->model));       
            else
                return json_encode(array('status'=>$r->result, 'pesan'=> $r->message));      
        }else{
            return json_encode(array('status'=>'false', 'pesan'=> 'Error'));        
        }
     
        

    }

 
}